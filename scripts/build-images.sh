#!/usr/bin/env bash

eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa

# Spring Demo
docker build \
  -t sedaq/spring-demo \
  git@gitlab.com:SedaQ/spring-demo.git#master

# React Frontend
docker build \
  -t sedaq/training-react-frontend \
  git@gitlab.com:SedaQ/react-demo.git#master